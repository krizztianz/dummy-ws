using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using dummyws.Models;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.IO;

namespace dummyws.Controllers
{
    public class AuthController : Controller
    {
        private IHostingEnvironment _env;
        public AuthController(IHostingEnvironment env)
        {
            _env = env;
        }

        [HttpPost]
        public IActionResult Login(FrmLogin frmLogin) {
            var userRepo = new UserRepository(this._env);
            var users = userRepo.GetAllUsers();

            Console.WriteLine(frmLogin.username + ' ' + frmLogin.password);

            var user = users.Where(a => a.Username == frmLogin.username && a.Password == frmLogin.password).FirstOrDefault();
            if(user != null) {
                user.SetisAuthenticated(true);
                Response.StatusCode = 200;
            }
            else {
                Response.StatusCode = 404;
            }
            
            var data = new {
                status = Response.StatusCode,
                collection = user
            };
            return Json(data);
        }
    }
}