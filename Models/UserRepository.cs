using System.Collections.Generic;
using System.IO;
using dummyws.Models;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

public class UserRepository {

    public IHostingEnvironment _env;

    public UserRepository(IHostingEnvironment env) {
        _env = env;
    }

    public List<User> GetAllUsers(){
        var webRoot = _env.WebRootPath;
            var file = System.IO.Path.Combine(webRoot, "Dummy/Users.json");
            var users = new List<User>(){};

            using (StreamReader readFile = File.OpenText(file))
            {
                JsonSerializer serializer = new JsonSerializer();
                List<User> listusers = (List<User>)serializer.Deserialize(readFile, typeof(List<User>));
                users = listusers;
            }

            return users;
    }
}