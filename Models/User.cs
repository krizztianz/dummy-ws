using System;

public class User {
    public string Username {get; set;}
    public string Password { get; set; }

    private Boolean isAuth;

    public Boolean GetisAuthenticated()
    {
        return isAuth;
    }

    public void SetisAuthenticated(Boolean value)
    {
        isAuth = value;
    }
}